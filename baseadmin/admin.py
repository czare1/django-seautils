#coding: utf-8

from django import template
from django.contrib import admin
from django.contrib.admin.options import IncorrectLookupParameters
from django.utils.encoding import force_unicode
from django.utils.translation import ungettext
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.conf.urls.defaults import patterns, url
from django.http import HttpResponseBadRequest
from django.db.models.signals import pre_save, post_save

from treebeard.admin import TreeAdmin

class SeaTreeAdmin(TreeAdmin):
    def move_pattern(self):
        """
        Url for admin drag&drop fixed move action
        """
        return patterns('',
            url('^move/$',
                self.admin_site.admin_view(self.move_node_fix),
                name="treebeard.ajax_move_node"),
        )

    def get_urls(self):
        """
        Adds a url to move nodes to this admin
        """
        urls = super(SeaTreeAdmin, self).get_urls()
        return self.move_pattern() + urls
    
    def move_node_fix(self, request):
        try:
            node_id = request.POST['node_id']
        except (KeyError, ValueError):
            # Some parameters were missing return a BadRequest
            return HttpResponseBadRequest(u'Malformed POST params')
        
        node = self.model.objects.get(pk=node_id)
        
        pre_save.send(node.__class__, instance=node, created=False)
        response = self.move_node(request)

        node = self.model.objects.get(pk=node_id)
        post_save.send(node.__class__, instance=node, created=False)
        
        return response

class BaseModelAdmin(admin.ModelAdmin):
    """
    Class where some useful common features can be implemented
    """
    
    change_list_template = 'baseadmin/change_list.html'

    def save_form(self, request, form, change):
        obj = super(BaseModelAdmin, self).save_form(request, form, change)
        if not change:
            obj.author = request.user
        obj.author_update = request.user
        return obj

    def get_fieldsets(self, request, *args, **kwargs):
        fieldsets = super(BaseModelAdmin, self).get_fieldsets(request, *args, **kwargs)
        # Remove field from fieldsets instead of exclude
        for fieldset in fieldsets:
            for field_name in getattr(self, 'hidden_rows', ()):
                fieldset[1]['fields'].remove(field_name)
        return fieldsets

    def simple_list_view(self, request, extra_context=None, 
                         list_display=None, 
                         template_path='', 
                         is_popup=True, 
                         queryset_modifier=lambda x: x):
        """
        The view which can display simple admin list.
        It does support any GET parameters.
        It DOES NOT support any POST parameters and therefore any list actions (like delete).
        The view is copied from django changelist_view
        """
        
        opts = self.model._meta
        app_label = opts.app_label
        
        if not list_display:
            list_display = list(self.list_display)
        
        list_display_links = self.get_list_display_links(request, list_display)
        # removing actions from list display
        try:
            list_display.remove('action_checkbox')
        except ValueError:
            pass
        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(request, self.model, list_display,
                list_display_links, self.list_filter, self.date_hierarchy,
                self.search_fields, self.list_select_related,
                self.list_per_page, self.list_max_show_all, self.list_editable,
                self)
        except IncorrectLookupParameters:
            pass
        
        # If we're allowing changelist editing, we need to construct a formset
        # for the changelist given all the fields to be edited. Then we'll
        # use the formset to validate/process POSTed data.
        formset = cl.formset = None
        cl.query_set = queryset_modifier(cl.get_query_set(request))
        cl.get_results(request)
        
        # Handle GET -- construct a formset for display.
        if request.method == "GET" and self.list_editable:
            form_set_class = self.get_changelist_formset(request)
            formset = cl.formset = form_set_class(queryset=cl.result_list)

        # Build the list of media to be used by the formset.
        if formset:
            media = self.media + formset.media
        else:
            media = self.media
            
        # Build the action form and populate it with available actions.
        action_form = None
        
        selection_note_all = ungettext('%(total_count)s selected',
            'All %(total_count)s selected', cl.result_count)

        context = {
            'module_name': force_unicode(opts.verbose_name_plural),
            'selection_note': _('0 of %(cnt)s selected') % {'cnt': len(cl.result_list)},
            'selection_note_all': selection_note_all % {'total_count': cl.result_count},
            'title': cl.title,
            'is_popup': is_popup,
            'cl': cl,
            'media': media,
            'has_add_permission': self.has_add_permission(request),
            #'root_path': self.admin_site.root_path,
            'app_label': app_label,
            'action_form': action_form,
            'actions_on_top': self.actions_on_top,
            'actions_on_bottom': self.actions_on_bottom,
            'actions_selection_counter': self.actions_selection_counter,
        }
        context.update(extra_context or {})
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response(template_path, context, context_instance=context_instance)
