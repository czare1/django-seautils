#coding: utf-8

from django.conf.urls.defaults import patterns, url
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

from django.core.urlresolvers import reverse

from meddo.utils.tastypie import tastypie_single_json, tastypie_json

class BootstrapAdmin(object):
    # class Meta:
    #     model
    #     form 
    #     resource 
    #     url_prefix 
    #     per_page 
    #     tpl_list 
    #     tpl_form 
    #     js_group 

    def urls(self):
        upr = self.Meta.url_prefix
        return patterns('',
            url(r'^%s/$' % upr, self.obj_form, name='badmin_%s.form' % upr),
            url(r'^%s/(?P<obj_id>\d+)/$' % upr, self.obj_form, name='badmin_%s.form' % upr),
            url(r'^$', self.obj_list, name='badmin_%s.list' % upr)
        )

    def obj_form(self, request, obj_id=None):
        mod = self.Meta.model
        data = {
            'js_group': self.Meta.js_group,
            'verbose_name': mod._meta.verbose_name,
            'verbose_name_plural': mod._meta.verbose_name_plural,
            'url_list': reverse('badmin_%s.list' % self.Meta.url_prefix)
        }
        if obj_id:
            cat = get_object_or_404(self.Meta.model, id=obj_id)
            article_form = self.Meta.form(instance=cat)
            data['initial_data'] = tastypie_single_json(request, self.Meta.resource, cat)
        else:
            article_form = self.Meta.form()
            cat = None
        data.update({'form': article_form})
        data.update(self.obj_form_additional_context(request, cat))
        return TemplateResponse(request, self.tpl_form(), data)

    def obj_form_additional_context(self, request, obj=None):
        return {}

    def tpl_form(self):
        return self.Meta.tpl_form if hasattr(self.Meta, 'tpl_form') else 'badmin_form.html' 

    def obj_list(self, request):
        data = {}
        objs = self.Meta.resource().obj_get_list(request)
        data.update({
                'objs': tastypie_json(request, self.Meta.resource, 
                    objs[:self.Meta.per_page]),
                'total_count': objs.count(),
                'model': self.Meta.model,
                'js_group': self.Meta.js_group
                })
        data.update(self.obj_list_additional_context(request))
        return TemplateResponse(request, self.Meta.tpl_list, data)

    def obj_list_additional_context(self, request):
        return {}

