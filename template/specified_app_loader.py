from os.path import dirname, join, abspath, isdir
 
from django.conf import settings
from django.db.models import get_app
from django.core.exceptions import ImproperlyConfigured
from django.template import TemplateDoesNotExist
from django.template.loader import BaseLoader
from django.utils._os import safe_join


class Loader(BaseLoader):
    is_usable = True

    def get_template_sources(self, template_name, template_dirs=None):
        """ 
        Template loader that only serves templates from specific app's template directory.
     
        Works for template_names in format app_label:some/template/name.html
        """
        if ":" in template_name:
            template_name, template_dir = self._get_template_vars(template_name)
            
            if template_name and template_dir and isdir(template_dir):
                try:
                    yield safe_join(template_dir, template_name)
                except UnicodeDecodeError:
                    raise
                except ValueError:
                    pass
    
    def load_template_source(self, template_name, template_dirs=None):
        tried = []
        for filepath in self.get_template_sources(template_name, template_dirs):
            try:
                template_file = open(filepath)
                try:
                    return (template_file.read().decode(settings.FILE_CHARSET), filepath)
                finally:
                    template_file.close()
            except IOError:
                tried.append(filepath)
        if tried:
            error_msg = "Tried %s" % tried
        else:
            error_msg = "Your TEMPLATE_DIRS setting is empty. Change it to point to at least one template directory."
        raise TemplateDoesNotExist(error_msg)
    
    def _get_template_vars(self, template_name):
        app_name, template_name = template_name.split(":", 1)
        try:
            template_dir = abspath(join(dirname(get_app(app_name).__file__), 'templates'))
        except ImproperlyConfigured:
            return None, None
        
        return template_name, template_dir

_loader = Loader()