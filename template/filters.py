#coding: utf-8

import re
from django.template import defaultfilters

SLUG_RE = r'[a-zA-Z0-9\-\+_\.\?]'

PG_MAX_INT = 2147483647

def slugify(value):
    return defaultfilters.slugify(re.sub(u"ł", "l", re.sub(u"Ł", "l", value)))
