#coding: utf-8

def pagination_range(current_page, num_pages, offset):
    """
    Method returns sliced links list.
    """
    
    if current_page <= offset:
        pagination_range = range(2, 2*offset+2)
    elif current_page >= num_pages-offset:
        pagination_range = range(num_pages-2*offset, num_pages)
    else:
        pagination_range = [1] + range(current_page-offset, current_page+offset+1) + [num_pages]
    pagination_range = sorted(list(set(pagination_range) & set(range(2,num_pages))))

    return {
        'init': 2 in pagination_range or pagination_range == [],
        'range': pagination_range,
        'tail': num_pages-1 in pagination_range or pagination_range == [],
    }
