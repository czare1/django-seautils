#coding: utf-8    
class ClassBasedView(object):
    @classmethod
    def as_view(cls):
        """
        Creates new instance each time is called.
        """
        def inner(*args, **kwargs):
            a = cls()
            return a(*args, **kwargs)
       
        return inner
    
    def __call__(self, *args, **kwargs):
        return self.view(*args, **kwargs)

    def view(self, *args, **kwargs):
        raise NotImplementedError

    def get_extra_context(self):
        """
        Extra data to be passed to template.
        Should return dict-like object.
        """
        return {}
    
    def get_template(self):
        return None
